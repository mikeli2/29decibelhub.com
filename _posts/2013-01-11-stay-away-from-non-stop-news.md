---
layout: post
title:  "Stay away from non stop news"
date:   2013-01-11 21:57:24
categories: thinking
---

# Stay away from non stop news
Wake up, iPhone on, open Zite app, which just refined it's UI, go through every interesting news,save some to Instapaper.Close,  open Flipboard, go twitter section, find out if there are interesting tools or websites or even jokes.Alright, finally go to HackerNews, find out what's new today, what's hottest startup or tools today.

2 hours later, repeat.
1 hours later, repeat.
…

We are surrounded by enormous news(well here I mean everything seems new to you) every moment, we fear to miss any piece of information which seems important to us. We spent every pieces of our life time on it, consume it, and forget it eventually.

I often ask myself, do I really need to know this? Do I really need to know that? Is that information will influence me or make my life better, in the most of time, obviously not.Most of time I just forget what I saw and still feel blank and tasteless.

From today, stay away from non stop news, read less articles, tweets, read more books, and, most of all, think more.
